SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

-- Drops
DROP TABLE IF EXISTS `Reservation`;
DROP TABLE IF EXISTS `MessageReserv`;
DROP TABLE IF EXISTS `MessagePublic`;
DROP TABLE IF EXISTS `Item`;
DROP TABLE IF EXISTS `Liste`;
DROP TABLE IF EXISTS `User`;
DROP TABLE IF EXISTS `Role`;

-- Creates
CREATE TABLE `Role` (
	`id` int(80) NOT NULL,
	`label` varchar(20),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `User` (
	`uid` int(80) NOT NULL AUTO_INCREMENT,
	`username` varchar(20) NOT NULL,
	`password` varchar(255) NOT NULL,
	`role_id` int(80) DEFAULT 1,
	PRIMARY KEY (`uid`)/*,
	FOREIGN KEY (`role_id`) REFERENCES Role (`id`) */
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `Liste` (
	`no` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) DEFAULT NULL,
	`titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`description` text COLLATE utf8_unicode_ci,
	`expiration` date DEFAULT NULL,
	`status` ENUM('private', 'public') DEFAULT 'private',
	`token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
	PRIMARY KEY (`no`)/*,
	FOREIGN KEY (`user_id`) REFERENCES User (`uid`) */
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `Item` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`liste_id` int(11) NOT NULL,
	`nom` text NOT NULL,
	`descr` text,
	`img` text,
	`url` text,
	`tarif` decimal(5,2) DEFAULT NULL,
	PRIMARY KEY (`id`)/*,
	FOREIGN KEY (`liste_id`) REFERENCES Liste (`no`)*/
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `MessagePublic` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`liste_id` int(11) NOT NULL,
	`content` text NOT NULL,
	`publication` date DEFAULT NULL,
	PRIMARY KEY (`id`)/*,
	FOREIGN KEY (`user_id`) REFERENCES User (`uid`),
	FOREIGN KEY (`liste_id`) REFERENCES Liste (`no`)*/
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `Reservation` (
	`id_res` int(11) NOT NULL AUTO_INCREMENT,
	`prenom` text NOT NULL,
	`nom` text NOT NULL,
    `message` text NOT NULL,
	`item_id` int(11) NOT NULL,
	PRIMARY KEY (`id_res`)/*,
	FOREIGN KEY (`item_id`) REFERENCES Item (`id`) */
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Inserts
INSERT INTO `Liste` (`no`, `user_id`, `titre`, `description`, `expiration`, `status`, `token`) VALUES
	(1, 1, 'Pour fêter le bac !', 'Pour un week-end à Nancy qui nous fera oublier les épreuves. ', '2018-06-27', 'public', '356a192b7913b04c54574d18c28d46e6395428ab'),
	(2, 1, 'Liste de mariage d''Alice et Bob', 'Nous souhaitons passer un week-end royal à Nancy pour notre lune de miel :)', '2018-06-30', 'private', 'da4b9237bacccdf19c0760cab7aec4a8359010b0'),
	(3, 2, 'C''est l''anniversaire de Charlie', 'Pour lui préparer une fête dont il se souviendra :)', '2017-12-12', 'public', '77de68daecd823babbb58edb1c8e14d7106e83bb');

INSERT INTO `Item` (`id`, `liste_id`, `nom`, `descr`, `img`, `url`, `tarif`) VALUES
	(1, 2, 'Champagne', 'Bouteille de champagne + flutes + jeux à gratter', 'champagne.jpg', '', 20.00),
	(2, 2, 'Musique', 'Partitions de piano à 4 mains', 'musique.jpg', '', 25.00),
	(3, 2, 'Exposition', 'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel', 'poirelregarder.jpg', '', 14.00),
	(4, 3, 'Goûter', 'Goûter au FIFNL', 'gouter.jpg', '', 20.00),
	(5, 3, 'Projection', 'Projection courts-métrages au FIFNL', 'film.jpg', '', 10.00),
	(6, 2, 'Bouquet', 'Bouquet de roses et Mots de Marion Renaud', 'rose.jpg', '', 16.00),
	(7, 2, 'Diner Stanislas', 'Diner à La Table du Bon Roi Stanislas (Apéritif / Entrée / Plat / Vin / Dessert / Café / Digestif)', 'bonroi.jpg', '', 60.00),
	(8, 3, 'Origami', 'Baguettes magiques en Origami en buvant un thé', 'origami.jpg', '', 12.00),
	(9, 3, 'Livres', 'Livre bricolage avec petits-enfants + Roman', 'bricolage.jpg', '', 24.00),
	(10, 2, 'Diner  Grand Rue ', 'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)', 'grandrue.jpg', '', 59.00),
	(11, 0, 'Visite guidée', 'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas', 'place.jpg', '', 11.00),
	(12, 2, 'Bijoux', 'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil', 'bijoux.jpg', '', 29.00),
	(19, 0, 'Jeu contacts', 'Jeu pour échange de contacts', 'contact.png', '', 5.00),
	(22, 0, 'Concert', 'Un concert à Nancy', 'concert.jpg', '', 17.00),
	(23, 1, 'Appart Hotel', 'Appart’hôtel Coeur de Ville, en plein centre-ville', 'apparthotel.jpg', '', 56.00),
	(24, 2, 'Hôtel d''Haussonville', 'Hôtel d''Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas', 'hotel_haussonville_logo.jpg', '', 169.00),
	(25, 1, 'Boite de nuit', 'Discothèque, Boîte tendance avec des soirées à thème & DJ invités', 'boitedenuit.jpg', '', 32.00),
	(26, 1, 'Planètes Laser', 'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.', 'laser.jpg', '', 15.00),
	(27, 1, 'Fort Aventure', 'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l''élastique inversé, Toboggan géant... et bien plus encore.', 'fort.jpg', '', 25.00);

INSERT INTO `Role` (`id`, `label`) VALUES 
	(1, 'basic user'),
	(2, 'admin');
	
INSERT INTO `User` (`uid`, `username`, `password`, `role_id`) VALUES
	(1, 'coucou', '$2y$12$rjwVDULmN8lKH/U.RK14Len0eiQNqS2bre3mbcdl2LbruafOKKIE2', 2),
	(2, 'heyhey', '$2y$12$7yWPtIfr6Xh./YmFeichguUheNcgEqZEkRiUSylCYf7PLeaeOpz2O', 2);
	
INSERT INTO `MessagePublic` (`user_id`, `liste_id`, `content`, `publication`) VALUES
	(1, 1, 'Salut c''est coucou ! Voici une de mes listes !', '2017-02-01'),
	(1, 2, 'Je me réjouie à l''avance de la réalisation de cette liste de souhait', '2017-08-18'),
	(2, 2, 'Salut, c''est heyhey !', '2017-12-15'),
	(2, 1, 'J''aime beaucoup ta liste de souhaits !', '2017-02-01'),
	(2, 2, 'PS : C''est heyhey.', '2017-08-18'),
	(1, 2, 'Salut, c''est coucou !', '2017-12-15');

INSERT INTO `Reservation` (`prenom`, `nom`, `message`, `item_id`) VALUES
	('Paul', 'Dupont', 'Je vous réserve cette magnifique bouteille de champagne', 1),
	('Jacques', 'Harris', 'Je réserve les origamis ^^', 8),
	('Pierre', 'Durant', 'Hey ;) je réserve la boîte de nuit !', 25),
	('Léo', 'Galassi', 'Salut les gars, je réserve pour Planètes Laser', 26);
