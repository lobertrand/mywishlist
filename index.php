﻿<?php
require_once 'vendor/autoload.php';

use mywishlist\controllers\ControleurListe;
use mywishlist\controllers\ControleurItem;
use mywishlist\controllers\Authentification;
use mywishlist\controllers\AuthException;
use mywishlist\controllers\DataException;
use mywishlist\views\VueErreur;
use mywishlist\models\Item;
use mywishlist\models\MessagePublic;
use Illuminate\Database\Capsule\Manager as DB;

session_start();

// Connexion
$db = new DB();
$username = parse_ini_file('conf.ini')['username'];
$password = parse_ini_file('conf.ini')['password'];
$db->addConnection([
	'driver' => 'mysql',
	'host' => 'localhost',
	'database' => 'bertra182u',
	'username' => $username,
	'password' => $password,
	'charset' => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix' => ''
]);
$db->setAsGlobal();
$db->bootEloquent();

// Initialisation de Slim
$app = new \Slim\Slim();

// App container

// Page d'accueil
$app->get('/', function () {
	Authentification::afficherAccueil();
})->name('accueil');

/*************
	COMPTE
*************/

// Processus d'incription
$app->post('/inscription', function () {
	global $app;
	try {
		
		$u = filter_var($_POST['username'], FILTER_SANITIZE_SPECIAL_CHARS);
		$p = filter_var($_POST['password'], FILTER_SANITIZE_SPECIAL_CHARS);
		$c = filter_var($_POST['confirm_password'], FILTER_SANITIZE_SPECIAL_CHARS);
		Authentification::sInscrire($u, $p, $c, Authentification::ADMIN);
		Authentification::seConnecter($u, $p);
		$app->redirect($app->urlFor('listes'));
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('inscription');

// Processus de connexion
$app->post('/connexion', function () {
	global $app;
	try {
		$u = filter_var($_POST['username'], FILTER_SANITIZE_SPECIAL_CHARS);
		$p = filter_var($_POST['password'], FILTER_SANITIZE_SPECIAL_CHARS);
		Authentification::seConnecter($u, $p);
		$app->redirect($app->urlFor('accueil'));				
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('connexion');

// Suppression du compte
$app->post('/suppressionCompte', function () {
	global $app;
	try {
		$p = filter_var($_POST['pass'], FILTER_SANITIZE_SPECIAL_CHARS);
		Authentification::supprimerCompte($p);
		$app->redirect($app->urlFor('accueil'));		
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('suppressionCompte');

// Changement de mot de passe
$app->post('/changementMotDePasse', function () {
	global $app;
	try {
		$ancien = filter_var($_POST['ancien'], FILTER_SANITIZE_SPECIAL_CHARS);
		$nouveau = filter_var($_POST['nouveau'], FILTER_SANITIZE_SPECIAL_CHARS);
		$confirm =  filter_var($_POST['confirm'], FILTER_SANITIZE_SPECIAL_CHARS);
		$msg = Authentification::changerMotDePasse($ancien, $nouveau, $confirm);
		$app->redirect($app->urlFor('accueil'));				
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('changementMotDePasse');

// Déconnexion
$app->get('/deconnexion', function () {
	global $app;
	Authentification::seDeconnecter();
	$app->redirect($app->urlFor('accueil'));
})->name('deconnexion');

// Affichage des paramètres du compte
$app->get('/compte', function () {
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
		Authentification::afficherCompte();		
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('compte');

/**************
	LISTES
**************/

// Affichage des listes
$app->get('/listes', function () {
	try {
		Authentification::verifierDroits(Authentification::ADMIN);				
		ControleurListe::afficherListes();
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('listes');

// Affichage des listes publiques
$app->get('/listespubliques', function () {
	try {
		ControleurListe::afficherListesPubliques();
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('listespubliques');

// Affichage du détail d'une liste de souhaits
$app->get('/listes/:id', function ($id) {
	try {
		ControleurItem::afficherListeDetail($id);
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('listeParId');

// generation url token
$app->get('/listepub/:token', function ($token) {
    try {
        ControleurItem::afficherListeDetailParToken($token);
    } catch (AuthException $e) {
        Authentification::afficherErreur($e);
    }
})->name('listeParToken');

// Affichage formulaire ajout Liste
$app->get('/ajoutliste', function () {
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
		ControleurListe::afficherFormulaireListe();
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('ajoutliste');

// Creation d'une nouvelle liste (insertion)
$app->post('/creationListe', function () {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
        ControleurListe::insertion();
        $app->redirect($app->urlFor('listes'));
    } catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('creationListe');

// Affichage formulaire modification Liste
$app->get('/modifListe/:no', function ($no) {
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
        ControleurListe::afficherFormulairemodif($no);
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('modifListe');

// Modification d'une Liste
$app->post('/modificationliste/:no', function ($no) {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
        ControleurListe::modification($no);
        $app->redirect($app->urlFor('listes'));
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('modificationliste');

// Suppression d'une liste
$app->get('/suppressionliste/:id', function ($id) {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
		ControleurListe::suppression($id);
		$app->redirect($app->urlFor('listes'));
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('suppressionliste');

// Processus d'ajout d'un commentaire sur une liste (avec d'id de la liste)
$app->post('/ajoutCommentaire/:id', function ($id) {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
		$content =  filter_var($_POST['commentaire'], FILTER_SANITIZE_SPECIAL_CHARS);
		ControleurListe::ajoutCommentaire($id, $content);
		$app->redirect($app->urlFor('listeParId', ['id'=>$id]));
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	} catch (DataException $e) {
		Authentification::afficherErreur($e);
	}
})->name('ajoutCommentaire');

// Processus de suppression d'un commentaire sur une liste (avec d'id du commentaire)
$app->get('/suppressionCommentaire/:id', function ($id) {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
		$liste = MessagePublic::where('id', '=', $id)->first();
		$liste_id;
		if(isset($liste)) {
			$liste_id = $liste->liste_id;
		}
		ControleurListe::suppressionCommentaire($id);
		$app->redirect($app->urlFor('listeParId', ['id'=>$liste_id]));
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	} catch (DataException $e) {
		Authentification::afficherErreur($e);
	}
})->name('suppressionCommentaire');

/*************
	ITEMS
*************/

// Affichage formulaire ajout item
$app->get('/ajoutitem/:liste_id', function ($liste_id) {
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
        ControleurItem::afficherFormulaireItem($liste_id);
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('ajoutitem');

// Creation d'un item (insertion)
$app->post('/insertionitem/:liste_id', function ($liste_id) {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
		ControleurItem::insertion($liste_id);
		$app->redirect($app->urlFor('listeParId', ['id' => $liste_id]));
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	} catch (DataException $e) {
		Authentification::afficherErreur($e);
	}
})->name('insertionitem');

// Affichage d'un item désigné par son ID
$app->get('/items/:id', function ($id) {
	try {
		ControleurItem::afficherItem($id);
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('itemParId');

// Affichage formulaire modification Item
$app->get('/modifItem/:id', function ($id_item) {
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
        ControleurItem::afficherFormulaireModifItem($id_item);
    } catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('modifItem');

// Modification Item
$app->post('/modificationItem/:id', function ($id_item) {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
        ControleurItem::modification($id_item);
		$app->redirect($app->urlFor('itemParId', ['id'=>$id_item]));
    } catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('modificationItem');

// Modification image Item
$app->post('/modificationImageItem/:id', function ($id_item) {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
		ControleurItem::modifierImage($id_item);
		$app->redirect($app->urlFor('itemParId', ['id'=>$id_item]));
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('modificationImageItem');

// Suppression image Item
$app->get('/suppressionImageItem/:id', function ($id_item) {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
		ControleurItem::supprimerImage($id_item);
		$app->redirect($app->urlFor('itemParId', ['id'=>$id_item]));
	} catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('suppressionImageItem');

// Suppression d'un item (delete)
$app->get('/suppressionItem/:id', function ($id_item) {
	global $app;
	try {
		Authentification::verifierDroits(Authentification::ADMIN);
		$liste_id = Item::where('id', '=', $id_item)->first()->liste_id;
        ControleurItem::suppression($id_item);
		$app->redirect($app->urlFor('listeParId', ['id'=>$liste_id]));
    } catch (AuthException $e) {
		Authentification::afficherErreur($e);
	}
})->name('suppressionItem');

// Formulaire reservation item
$app->get('/formulairereservationItem/:id', function ($id_item) {
    global $app;
    $liste_id = Item::where('id', '=', $id_item)->first()->liste_id;
    ControleurItem::formReservation($id_item);
})->name('formulairereservationItem');

// Réservation d'un item
$app->post('/reservationItem/:id', function ($id_item) {
    global $app;     
    $liste_id = Item::where('id', '=', $id_item)->first()->liste_id;
    ControleurItem::reservation($id_item);
    $app->redirect($app->urlFor('itemParId', ['id'=>$id_item])); 
})->name('reservationItem');


// Exécution de Slim
$app->run();
