<?php
namespace mywishlist\models;

class User extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'User';
	protected $primaryKey = 'uid';
	protected $guarded = array();
	public $timestamps = false;
	
	public function role() {
        return $this->belongsTo('mywishlist\models\Role', 'role_id');
    }
	
}