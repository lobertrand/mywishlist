<?php
namespace mywishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'Liste';
	protected $primaryKey = 'no';
	protected $guarded = array();
	public $timestamps = false;
	
	public function items() {
		return $this->hasMany('mywishlist\models\Item', 'liste_id');
	}
	
}