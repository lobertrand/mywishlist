<?php
namespace mywishlist\models;

class MessagePublic extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'MessagePublic';
	protected $primaryKey = 'id';
	protected $guarded = array();
	public $timestamps = false;
	
	public function liste() {
        return $this->belongsTo('mywishlist\models\Liste', 'liste_id');
    }
	
	public function auteur() {
		return $this->belongsTo('mywishlist\models\User', 'user_id');
	}
	
}