<?php
namespace mywishlist\models;

class Item extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'Item';
    protected $primaryKey = 'id';
	protected $guarded = array();
    public $timestamps = false;

    public function liste() {
        return $this->belongsTo('mywishlist\models\Liste', 'liste_id');
    }
    public function reservation() {
        return $this->hasMany('mywishlist\models\Reservation', 'id');
    }
}
