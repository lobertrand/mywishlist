<?php
namespace mywishlist\models;

class Reservation extends \Illuminate\Database\Eloquent\Model {
    
    protected $table = 'Reservation';
    protected $primaryKey = 'id_res';
    protected $guarded = array();
    public $timestamps = false;
    
    public function items() {
        return $this->belongsTo('mywishlist\models\Item', 'id');
    }
}
