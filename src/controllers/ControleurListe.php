<?php
namespace mywishlist\controllers;

use mywishlist\models\Liste;
use mywishlist\models\MessagePublic;
use mywishlist\models\Item;
use mywishlist\views\VueCreateur;
use mywishlist\views\VueFormulaireAJoutListe;
use mywishlist\views\VueParticipant;


class ControleurListe {
    
	public static function afficherListes() {
		$uid = $_SESSION['profile']['uid'];
		$listes = Liste::where('user_id','=', $uid)->get();
		
		$v;
		if (isset($uid)) {
			$v = new VueCreateur([$listes], VueCreateur::VUE_LISTES);
		} else {
			$v = new VueParticipant([$listes], VueParticipant::VUE_LISTES);
		}
		$v->render();
	}
	
	public static function afficherListesPubliques() {
		$listes = Liste::where('status','=', 'public')->get();
		
		$v;
		if (isset($_SESSION['profile'])) {
			$v = new VueCreateur([$listes], VueCreateur::VUE_LISTESPUBLIQUES);
		} else {
			$v = new VueParticipant([$listes], VueParticipant::VUE_LISTESPUBLIQUES);
		}
		
		$v->render();
	}
	
	public static function afficherFormulaireListe() {
		$v=new VueCreateur([], VueCreateur::VUE_FORMULAIREAJOUTLISTE);
	    $v->render();
	}
	
	public static function insertion() {
	    $liste = new Liste();

		$titre =  filter_var($_POST['nomListe'], FILTER_SANITIZE_SPECIAL_CHARS);
		$description = filter_var($_POST['descrListe'], FILTER_SANITIZE_SPECIAL_CHARS);
		$expiration = filter_var($_POST['expiration'], FILTER_SANITIZE_SPECIAL_CHARS);
		$status = filter_var($_POST['status'], FILTER_SANITIZE_SPECIAL_CHARS);
		
		$liste->user_id = $_SESSION['profile']['uid'];
	    $liste->titre = $titre;
	    $liste->description = $description;
		$liste->expiration = $expiration;
		$liste->status = $status;
		
	    $liste->save();
		
		// Après insertion, on récupère l'id de la liste pour créer le token
		$token = sha1($liste->no);
		$liste->update(["token"=>$token]);
	}
	
	public static function afficherFormulairemodif($no) {
		$uid = $_SESSION['profile']['uid'];
		$liste = Liste::where('no', '=', $no)->first();
		if ($uid != $liste->user_id) {
			throw new AuthException("Erreur: cette liste ne vous appartient pas.", AppException::RETOUR_MESLISTES);
		}
		
	    $v=new VueCreateur([$liste], VueCreateur::VUE_FORMULAIREMODIFLISTE);
	    $v->render();
	}
	
	public static function modification($no_liste) {
	    $liste=Liste::where("no", "=", $no_liste)->first();
	    $uid = $_SESSION['profile']['uid'];
		if ($uid != $liste->user_id) {
			throw new AuthException("Erreur: cette liste ne vous appartient pas.", AppException::RETOUR_MESLISTES);
		}
	    $titre =  filter_var($_POST['nomListe'], FILTER_SANITIZE_SPECIAL_CHARS);
	    $description = filter_var($_POST['descrListe'], FILTER_SANITIZE_SPECIAL_CHARS);
	    $expiration = filter_var($_POST['expiration'], FILTER_SANITIZE_SPECIAL_CHARS);
	    $status = filter_var($_POST['status'], FILTER_SANITIZE_SPECIAL_CHARS);
	    $liste->update(["titre"=>$titre]);
	    $liste->update(["description"=>$description]);
	    $liste->update(["expiration"=>$expiration]);
	    $liste->update(["status"=>$status]);
	}
	
	public static function suppression($no_liste) {
	    $liste=Liste::where("no", "=", $no_liste)->first();
	    $uid = $_SESSION['profile']['uid'];
		if ($uid != $liste->user_id) {
			throw new AuthException("Erreur: cette liste ne vous appartient pas.", AppException::RETOUR_MESLISTES);
		}
		
		$commentaires = MessagePublic::where("liste_id", "=", "$no_liste")->get();
		foreach ($commentaires as $comm) {
			$comm->delete();
		}
		$items = Item::where("liste_id", "=", "$no_liste")->get();
		foreach ($items as $it) {
			$it->delete();
		}
	    $liste->delete();
	}
	
	public static function ajoutCommentaire($liste_id, $content) {
		if ($content == "") {
			throw new DataException("Erreur: Votre commentaire est vide", AppException::RETOUR_PAGEPRECEDENTE);
		}
		
		$msg = new MessagePublic();
		$msg->user_id = $_SESSION['profile']['uid'];
		$msg->liste_id = $liste_id;
		$msg->content = filter_var($content, FILTER_SANITIZE_SPECIAL_CHARS);
		$msg->publication = date("Y-m-d H:i:s");
		$msg->save();
	}
	
	public static function suppressionCommentaire($com_id) {
		$msg = MessagePublic::where('id', '=', $com_id)->first();
		if (!isset($msg)) {
			throw new AuthException("Erreur: ce commentaire n'existe pas :(", AppException::RETOUR_PAGEPRECEDENTE);
		}
		$user_id = MessagePublic::where('id', '=', $com_id)->first()->user_id;
		if ($_SESSION['profile']['uid'] != $user_id) {
			throw new AuthException("Erreur: ce commentaire ne vous appartient pas", AppException::RETOUR_PAGEPRECEDENTE);
		}
		MessagePublic::where('id', '=', $com_id)->delete();
	}
	
}
