<?php
namespace mywishlist\controllers;

use mywishlist\models\Liste;
use mywishlist\models\Item;
use mywishlist\models\Reservation;
use mywishlist\models\MessagePublic;
use mywishlist\views\VueCreateur;
use mywishlist\views\VueParticipant;

class ControleurItem {
	/**
	 * Affiche une liste partagée grace à un token dans une VueParticipant
	 */
	public static function afficherListeDetailParToken($token) {
		$liste = Liste::where('token', "=", $token)->first();
		
		if (!isset($liste)) {
			throw new AuthException("Oups, cette liste n'existe pas :/", AppException::RETOUR_MESLISTES);
		}
		
        $items = $liste->items;
		$uid = $liste->user_id;
        
		$coms = MessagePublic::where('liste_id', '=', $liste->no)->get();
		
        $v = new VueParticipant([
            $liste,
            $items,
			$coms
        ], VueParticipant::VUE_LISTEDETAIL);
        $v->render();
	}

    public static function afficherListeDetail($no_liste) {		
        $liste = Liste::where('no', "=", $no_liste)->first();

		if (!isset($liste)) {
			throw new AuthException("Oups, cette liste n'existe pas :/", AppException::RETOUR_MESLISTES);
		}
		
        $items = $liste->items;
		$uid = $liste->user_id;
		if (isset($_SESSION['profile'])) {
			$active_uid = $_SESSION['profile']['uid'];
			if ($uid != $active_uid && $liste->status == 'private') {
				throw new AuthException("Oups, cette liste n'est pas à vous :/", AppException::RETOUR_MESLISTES);
			}
		} else {
			if ($liste->status == 'private') {
				throw new AuthException("Oups, cette liste n'est pas à vous :/", AppException::RETOUR_ACCUEIL);
			}
		}
       
		$coms = MessagePublic::where('liste_id', '=', $liste->no)->get();
		
		$v;
		if (isset($_SESSION['profile'])) {
			$v = new VueCreateur([
				$liste,
				$items,
				$coms
			], VueCreateur::VUE_LISTEDETAIL);
		} else {
			$v = new VueParticipant([
				$liste,
				$items,
				$coms
			], VueParticipant::VUE_LISTEDETAIL);
		}
        
        $v->render();
    }
    
    public static function afficherListePubliqueDetail($no_liste) {		
        $liste = Liste::where('no', "=", $no_liste)->first();
		
		if (!isset($liste)) {
			throw new AuthException("Oups, cette liste n'existe pas :/", AppException::RETOUR_MESLISTES);
		}
		
        $items = $liste->items;
		$uid = $liste->user_id;
		$active_uid = $_SESSION['profile']['uid'];
		
		if ($uid != $active_uid && $liste->status == 'prive') {
			throw new AuthException("Oups, cette liste n'est pas à vous :/", AppException::RETOUR_MESLISTES);
		}
        
		$coms = MessagePublic::where('liste_id', '=', $liste->no)->get();
		
        $v = new VueCreateur([
            $liste,
            $items,
			$coms
        ], VueCreateur::VUE_LISTEPUBLIQUESDETAIL);
        $v->render();
    }

    public static function afficherItem($id_item) {
        $item = Item::where('id', '=', $id_item)->first();
		
		if (!isset($item)) {
			throw new AuthException("Oups, cet item n'existe pas :/", AppException::RETOUR_MESLISTES);
		}
		
        $uid = $item->liste->user_id;
        
        $liste_cour = Liste::where('no', "=", $item->liste_id)->first();
		
		if (isset($_SESSION['profile'])) {
			$active_uid = $_SESSION['profile']['uid'];
			
			if($uid != $active_uid && $liste_cour->status == 'prive') {
				throw new AuthException("Oups, cet item n'est pas à vous :/", AppException::RETOUR_MESLISTES);
			}
		}
		$v;
		if (isset($_SESSION['profile'])) {
			$v = new VueCreateur([
				$item
			], VueCreateur::VUE_ITEM);
		} else {
			$v = new VueParticipant([
				$item
			], VueParticipant::VUE_ITEM);
		}
		
        $v->render();
    }

    public static function afficherFormulaireItem($liste_id) {
		$liste = Liste::where('no', '=', $liste_id)->first();
		if($liste->user_id != $_SESSION['profile']['uid']) {
			throw new AuthException("Opération impossible : cette liste n'est pas à vous", AuthException::RETOUR_MESLISTES);
		}
		
		$liste = Liste::where('no', '=', $liste_id)->first();
        $v = new VueCreateur([$liste], VueCreateur::VUE_FORMULAIREITEM);
        $v->render();
    }

    public static function insertion($liste_id) {
        $liste = Liste::where('no', '=', $liste_id)->first();
		if($liste->user_id != $_SESSION['profile']['uid']) {
			throw new AuthException("Oups, création impossible : cette liste n'est pas à vous", AuthException::RETOUR_MESLISTES);
		}
		
        $item = new Item();
		$item->liste_id = $liste_id;
        $item->nom = filter_var($_POST['nomItem'], FILTER_SANITIZE_SPECIAL_CHARS);
        $item->descr = filter_var($_POST['descrItem'], FILTER_SANITIZE_SPECIAL_CHARS);
		
		// Pour image de l'ordinateur
		$uploaddir = 'web/img/';
		$basename = basename($_FILES['image']['name']);
		if (!self::isImageFile($basename)) {
			throw new DataException("Erreur : les formats d'images acceptés sont .png .jpeg .jpg .gif et .bmp", AppException::RETOUR_PAGEPRECEDENTE);
		}
		$uploadfile = $uploaddir . $basename;
		if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
			$item->img = $_FILES['image']['name'];
		} else {
 			$item->img = 'noimage.png';
		}
		
		$tarif = $_POST['tarif'];
		if (!isset($tarif)) {
			$item->tarif = 0;
		} else {
			$item->tarif = $tarif;
		}
		 
        $item->save();		
    }
    
    public static function afficherFormulaireModifItem($id_item) {
		$item = Item::where('id', '=', $id_item)->first();
		
		$uid = $item->liste->user_id;
	    if($uid != $_SESSION['profile']['uid']) {
			throw new AuthException("Oups, cet item n'est pas à vous :/", AuthException::RETOUR_MESLISTES);
		}
		
		$v=new VueCreateur([$item], VueCreateur::VUE_FORMULAIREMODIFITEM);
	    $v->render();	
	}

	public static function modification($id_item) {
	    $item=Item::where("id", "=",  $id_item)->first();
	    
	    $uid = $item->liste->user_id;
	    if($uid != $_SESSION['profile']['uid']) {
			throw new AuthException("Oups, cet item n'est pas à vous :/", AuthException::RETOUR_MESLISTES);
		}
		
	    $nom =  filter_var($_POST['nomItem'], FILTER_SANITIZE_SPECIAL_CHARS);
	    $descr = filter_var($_POST['descrItem'], FILTER_SANITIZE_SPECIAL_CHARS);
	    $tarif = filter_var($_POST['tarif'], FILTER_SANITIZE_SPECIAL_CHARS);
		
	    $item->update(["nom"=>$nom]);
	    $item->update(["descr"=>$descr]);
	    $item->update(["tarif"=>$tarif]);
	}
	
	public static function suppression($id_item) {
		$item = Item::where('id', '=', $id_item)->first();
		
		$uid = $item->liste->user_id;
	    if($uid != $_SESSION['profile']['uid']) {
			throw new AuthException("Oups, cet item n'est pas à vous :/", AuthException::RETOUR_MESLISTES);
		}
		$item->delete();
	}
	
	public static function isImageFile($url) {
		$accepted_formats = array('png', 'jpeg', 'jpg', 'gif', 'bmp', '');
		$tab = explode('.', $url);
		$extension = $tab[sizeof($tab)-1];
		foreach ($accepted_formats as $format) {
			if ($format == $extension) {
				return true;
			}
		}
		return false;
	}
	
	public static function modifierImage($id_item){
		$item = Item::where('id', "=", $id_item)->first();
		$uploaddir = 'web/img/';
		if (isset($_FILES['image_remplacement'])) {
			$basename = basename($_FILES['image_remplacement']['name']);
			if (!self::isImageFile($basename)) {
				throw new DataException("Erreur : les formats d'images acceptés sont .png .jpeg .jpg .gif et .bmp", AppException::RETOUR_PAGEPRECEDENTE);
			}
			$uploadfile = $uploaddir . $basename;
			if (move_uploaded_file($_FILES['image_remplacement']['tmp_name'], $uploadfile)) {
				$item->img = $_FILES['image_remplacement']['name'];
			} else {
				$item->img = 'noimage.png';
			}
			$item->save();
		}
	}

	public static function supprimerImage($id_item){
		$item = Item::where('id', "=", $id_item)->first();
		$item->img = 'noimage.png';
		$item->save();
	}
	
	public static function reservation($id_item) {
		$reservation = new Reservation();
		$nom =  filter_var($_POST['Nom'], FILTER_SANITIZE_SPECIAL_CHARS);
		$prenom = filter_var($_POST['Prenom'], FILTER_SANITIZE_SPECIAL_CHARS);
		$message = filter_var($_POST['Message'], FILTER_SANITIZE_SPECIAL_CHARS);
		$reservation->nom=$nom;
		$reservation->prenom=$prenom;
		$reservation->message=$message;
		$reservation->item_id=$id_item;
		$reservation->save();
	}
	
	public static function formReservation($id_item) {
		
		$item = Item::where('id', '=', $id_item)->first();
		$v;
		if (isset($_SESSION['profile'])) {
			$v = new VueCreateur([$item], VueCreateur::VUE_FORMRESERVATION);
		} else {
			$v = new VueParticipant([$item], VueParticipant::VUE_FORMRESERVATION);
		}
		
		$v->render();	
	}
		
}
