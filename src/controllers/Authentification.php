<?php
namespace mywishlist\controllers;

use mywishlist\views\VueAccueil;
use mywishlist\views\VueErreur;
use mywishlist\views\VueCreateur;
use mywishlist\models\User;
use mywishlist\models\Liste;
use mywishlist\models\Item;

class Authentification {

	const BASIC_USER = 1;
	const ADMIN = 2;
	
	public static function afficherAccueil() {
		$listes_publiques = Liste::where('status','=', 'public')->get();
		$v = new VueAccueil($listes_publiques);
		$v->render();
	}
	
	public static function afficherErreur($e) {
		$msg = $e->getMessage();
		$type_retour = $e->getRetour();
		$v = new VueErreur($msg, $type_retour);
		$v->render();
	}
	
	public static function afficherCompte() {
		$v = new VueCreateur([], VueCreateur::VUE_COMPTE);
		$v->render();
	}

	/**
	 * Crée un nouveau compte en vérifiant l'inexistance du pseudo
	 * et la confirmation du mot de passe.
	 */
	public static function sInscrire($uname, $pass, $confirm_pass, $niv) {
		// Pseudo < 4 chars ou contient autre chars que des lettres et chiffres
		if (strlen($uname) < 4 || preg_match("/^[a-zA-Z0-9]+$/", $uname) != 1) {
			throw new AuthException("Erreur : Le pseudo doit faire au moins 4 caractères et ne doit contenir que des chiffres et/ou des lettres.", AppException::RETOUR_ACCUEIL);
		}
		// Pseudo déjà existant
		$u = User::where('username', '=', $uname)->first();
		if (isset($u)) {
			throw new AuthException("Erreur : Cet identifiant est déjà pris.", AppException::RETOUR_ACCUEIL);
		}
		// Mot de passe < 6 chars
		if (strlen($pass) < 6) {
			throw new AuthException("Erreur : Le mot de passe doit faire au moins 6 caractères", AppException::RETOUR_ACCUEIL);
		}
		// Mot de passe de confirmation incorrect
		if ($pass != $confirm_pass) {
			throw new AuthException("Erreur : Les mots de passe entrés sont différents.", AppException::RETOUR_ACCUEIL);
		}
		
		// Si pas d'erreur
		$hash = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 12]);
		
		$h = new User();
		$h->username = $uname;
		$h->password = $hash;
		$h->role_id = $niv;
		$h->save(); // Insertion dans la table User
	}
	
	/**
	 * Supprime le compte courant
	 */
	public static function supprimerCompte($pass) {
		$uid = $_SESSION['profile']['uid'];
		$user = User::where('uid', '=', $uid)->first();
		$registered_hash = $user->password;
		
		// Le mot de passe est incorrect
		if (!password_verify($pass, $registered_hash)) {
			throw new AuthException("Erreur : Le mot de passe est incorrect", AppException::RETOUR_COMPTE);
		}
		self::seDeconnecter();
		
		User::where('uid', '=', $uid)->delete();
		
		// Supprimer les listes de l'utilisateur et tous leurs items
		$listes = Liste::where('user_id', '=', $uid)->get();
		foreach($listes as $l) {
			$no = $l->no;
			Item::where('liste_id', '=', $no)->delete();
		}
		Liste::where('user_id', '=', $uid)->delete();
		
	}

	/**
	 * Connecte un utilisateur à son compte en comparant sont mot
	 * de passe avec celui enregistré dans la base.
	 */
	public static function seConnecter($uname, $pass) {
		$msg_err = "Erreur d'identifiant ou de mot de passe.";
		
		// Le pseudo est inconnu
		$user = User::where('username', "=", $uname)->first();
		if (!isset($user)) {
			throw new AuthException($msg_err, AppException::RETOUR_ACCUEIL);
		}
		
		// Le mot de passe est incorrect
		$registered_hash = $user->password;
		if (!password_verify($pass, $registered_hash)) {
			throw new AuthException($msg_err, AppException::RETOUR_ACCUEIL);
		}
		
		$uid = $user->uid;
		$role_id = $user->role_id;
		
		self::seDeconnecter();
		
		// Création de la variable de session
		$_SESSION['profile'] = array(
			'username' => $uname,
			'uid' => $uid,
			'role' => $role_id
		);
	}
	
	public static function changerMotDePasse($ancien, $nouv, $confirm) {
		$uname = $_SESSION['profile']['username'];
		$user = User::where('username', "=", $uname)->first();
		$registered_hash = $user->password;
		// Vérifier si l'ancien mot de passe est correct
		// Le mot de passe est incorrect
		if (!password_verify($ancien, $registered_hash)) {
			throw new AuthException("L'ancien mot de passe est incorrect", AppException::RETOUR_COMPTE);
		}
		// Vérifier que le nouveau respecte les critères de sécurité
		// Mot de passe < 6 chars
		if (strlen($nouv) < 6) {
			throw new AuthException("Erreur : Le nouveau mot de passe doit faire au moins 6 caractères.", AppException::RETOUR_COMPTE);
		}
		// Vérifier que le celui de confirmation est correct
		// Mot de passe de confirmation incorrect
		if ($nouv != $confirm) {
			throw new AuthException("Erreur : Les mots de passe entrés sont différents.", AppException::RETOUR_COMPTE);
		}
		
		$hash = password_hash($nouv, PASSWORD_DEFAULT, ['cost' => 12]);
		$user->password = $hash;
		$user->save(); // Insertion dans la table User
		
		self::seDeconnecter();
	}
	
	/**
	 * Libère la variable de session
	 */
	public static function seDeconnecter() {
		unset($_SESSION['profile']);
	}
	
	/**
	 * Vérifie si un utilisateur est connecté et si ses droits de
	 * sont suffisants pour effectuer une action.
	 */
	public static function verifierDroits($min_expected) {
		// Aucun utilisateur connecté
		if (!isset($_SESSION['profile'])) {
			throw new AuthException("Vous devez être connecté pour accéder à cette page", AppException::RETOUR_ACCUEIL);
		}
		// Droits insuffisants
		$droits_util = $_SESSION['profile']['role'];
		if ($droits_util < $min_expected) {
			throw new AuthException("Vous n'avez pas des droits suffisants pour accéder à cette page", AppException::RETOUR_ACCUEIL);
		}
	}
	
}
