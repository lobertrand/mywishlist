<?php
namespace mywishlist\controllers;

class PathFinder {
	
	/**
	 * Méthode créant l'url relative vers un fichier (css par exemple) 
	 * en fonction de la route actuelle
	 *
	 * @param string $url
	 *        	Fin du chemin, à partir de la racine du projet
	 *        	(exemple : "web/css/style.css")
	 * @return string Url relative recomposée
	 */
	public static function findPath($url) {
		$app = \Slim\Slim::getInstance();
		$routeActuelle = $app->request->getPathInfo();
		$nbRetours = substr_count($routeActuelle, '/') - 1;
		$retours = "";
		for ($i = 0; $i < $nbRetours; $i ++) {
			$retours .= '../';
		}
		return $retours . $url;
	}
	
}
