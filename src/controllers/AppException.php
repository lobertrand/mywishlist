<?php
namespace mywishlist\controllers;

class AppException extends \Exception {
    
	/**
	 * Indique qu'un retour à la page précédente est conseillé
	 */
	const RETOUR_PAGEPRECEDENTE = 1;
	/**
	 * Indique qu'un retour à l'écran d'accueil est conseillé
	 */
	const RETOUR_ACCUEIL = 2;
	/**
	 * Indique qu'un retour à l'écran de paramétrage du compte est conseillé
	 */
	const RETOUR_COMPTE = 3;
	/**
	 * Indique qu'un retour à la page "Mes Listes" est conseillé
	 */
	const RETOUR_MESLISTES = 4;
	
	/*
	 * Type de retour conseillé selon l'exception
	 */
	private $retour;
	
	function __construct($message, $retour) {
		parent::__construct($message, 0);
		$this->retour = $retour;
	}
	
	function getRetour() {
		return $this->retour;
	}
	
}