<?php
namespace mywishlist\views;

class VueFormulaireModifItem extends VueAbstraite
{

	private $item;

    public function __construct($item) {
		$this->item = $item;
	}

    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $id = $this->item->id;
		$item = $this->item;
		$modification = $app->urlFor("modificationItem", ["id"=>$id]);	
		$suppression = $app->urlFor("suppressionImageItem", ["id"=>$id]);
		$html = <<<END

		<div class="container">

			<h2>Modifier l'item</h2>
			<form id="f2" method="post" action="$modification" class="card">
				
				<label for="f1_name">Nom</label>
				<input type="text" name="nomItem" value="$item->nom" required autofocus>

				<label for="f1_descr">Description</label>
				<input type="text" name="descrItem" value="$item->descr" required>

				<!--<label for="f1_url">Url</label>
				<input type="url" name="url">
				
				<label for="f1_image">Image de votre ordinateur</label>
				<input type="file" name="image" value="$item->img">//-->

				<label for="f1_tarif">Tarif</label>
				<input type="number" name="tarif" value="$item->tarif" step="0.01" required>
				
				<div class="card_footer">
					<button type="submit">Valider</button>
				</div>
			</form>
		</div>
		
END;
        return $html;
    }
}
