<?php
namespace mywishlist\views;

use mywishlist\controllers\AppException;
use mywishlist\controllers\PathFinder;

class VueErreur extends VueAbstraite {
	
	private $message;
	
	public function __construct($message, $retour) {
		$this->message = $message;
		$this->retour = $retour;
	}
	
	public function render() {
		$app = \Slim\Slim::getInstance();
		$msg = $this->message;
		
		$btn_retour = "";
		switch ($this->retour) {
			case AppException::RETOUR_ACCUEIL:
				$url = $app->urlFor('accueil');
				$btn_retour = <<<END
					<a class="btn" href="$url" autofocus>
						<i class="material-icons">home</i>
						<span>Retour à l'accueil</span>
					</a>
END;
			break;
			case AppException::RETOUR_COMPTE:
				$url = $app->urlFor('compte');
				$btn_retour = <<<END
					<a class="btn" href="$url" autofocus>
						<i class="material-icons">account_circle</i>
						<span>Retour au compte</span>
					</a>
END;
			break;
			case AppException::RETOUR_MESLISTES:
				$url = $app->urlFor('listes');
				$btn_retour = <<<END
					<a class="btn" href="$url" autofocus>
						<i class="material-icons">view_list</i>
						<span>Retour à mes listes</span>
					</a>
END;
			break;
			case AppException::RETOUR_PAGEPRECEDENTE:
				$url = "javascript:history.go(-1)";
				$btn_retour = <<<END
					<a class="btn" href="$url" autofocus>
						<i class="material-icons">undo</i>
						<span>Retour à la page précédente</span>
					</a>
END;
			break;
		}
		
		$css = PathFinder::findPath('web/css/style.css');
		
		$html = <<<END
		
		<!DOCTYPE html>
		<html lang="fr">

		<head>
			<title>MyWishList</title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<link rel="stylesheet" href="$css" type="text/css">
			<link rel="icon" type="image/png" href="web/img/favicon.png" />
		</head>

		<body>
			<div class="msg_err landing_page">
				<div class="container">
					<h1>MyWishList</h1>
					<p>$msg</p>
					$btn_retour
				</div>
			</div>
		</body>

		</html>


END;
		echo $html;
	}
	
}