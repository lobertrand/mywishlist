<?php
namespace mywishlist\views;

use mywishlist\controllers\PathFinder;

class VueListes extends VueAbstraite {

	private $tab;
	
	public function __construct($tab) {
		$this->tab = $tab;
	}

	public function render() {
		$app = \Slim\Slim::getInstance();
		
		$si_vide = "";
		if(count($this->tab) == 0) {
			$si_vide = "<p class=\"aucune_liste\">Vous n'avez aucune liste pour le moment.</p>";
		}

		$html = <<<END
		<div class="container">
			<h2>Mes listes de souhaits</h2>
			$si_vide
			<ul>
END;
		foreach ($this->tab as $liste) {
			$no = $liste->no;
			$voirItems = $app->urlFor('listeParId', ['id'=>$no]);
            $modification = $app->urlFor('modifListe', ['no'=>$no]);
            $supprimer = $app->urlFor('suppressionliste', ['id'=>$no]);
			
			$affichStatus = '';
			if ($liste->status != 'private') {
				$affichStatus = 'Cette liste est publique';
			} else {
				$affichStatus = 'Cette liste est privée';
			}
			
			$lienPartageable = $this->getUrlPartage($liste->token);
			$clipboardjs = PathFinder::findPath('lib/clipboard.min.js');
			
			$html .= <<<END

				<li class="card">
					<h3>$liste->titre</h3>
					<p>$liste->description</p>
                    
                    <div class="card_footer">
						<p class="expiration">Expire le $liste->expiration -- $affichStatus</p>
						<a href="$voirItems">Voir les items</a>
                        <a href="$modification">Modifier liste</a>
                        <a href="$supprimer">Supprimer liste</a>
                        <button onclick="showBox('partage_box$no')">Partager</button>
					</div>
				</li>
				
				<div id="partage_box$no" class="lightbox">
					<div class="lightbox_header">
						<h2>Partager cette liste</h2>
						<button onclick="closeBox('partage_box$no')" class="close">
							<i class="material-icons">close</i>
						</button>
					</div>
					<div class="card">			
						<label for="lien_partage">Copiez ce lien et envoyez-le à qui vous souhaitez</label>
						<input id="lien_partage$no" type="text" value="$lienPartageable">
						<div class="card_footer">
							<button class="copy_btn" data-clipboard-target="#lien_partage$no">Copier dans le presse-papier</button>
						</div>
					</div>
				</div>
				
				<script src="web/js/lib/clipboard.min.js"></script>
				<script>
					new Clipboard('.copy_btn');
				</script>
END;
		}
		$html .= "
			</ul>
		</div>";
		return $html;
	}
	
	public function getUrlPartage($token) {
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$lien_partage = str_replace('/listes', '/listepub/', $actual_link) . $token;
		return $lien_partage;
	}
	
}
