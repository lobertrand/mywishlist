<?php
namespace mywishlist\views;

use mywishlist\models\User;

class VueListesPubliques extends VueAbstraite {

	private $tab;
	
	public function __construct($tab) {
		$this->tab = $tab;
	}

	public function render() {
		$app = \Slim\Slim::getInstance();
		
		$si_vide = "";
		if(count($this->tab) == 0) {
			$si_vide = "<p class=\"aucune_liste\">Aucune liste publique n'existe pour le moment.</p>";
		}

		$html = <<<END
		<div class="container">
			<h2>Listes de souhaits publiques</h2>
			$si_vide
			<ul>
END;
		foreach ($this->tab as $liste) {
			$no = $liste->no;
			$voirItems = $app->urlFor('listeParId', ['id'=>$no]);
			$urlModification = $app->urlFor('modifListe', ['no'=>$no]);
			$urlSupprimer = $app->urlFor('suppressionliste', ['id'=>$no]);
			$modifButton = "";
			$supprButton = "";
			
			$createur = User::where('uid', '=', $liste->user_id)->first();
			
			
			if (isset($_SESSION['profile']) && $_SESSION['profile']['uid'] == $liste->user_id) {
				$modifButton = '<a href=' . "$urlModification" . '>Modifier liste</a>';
				$supprButton = '<a href=' . "$urlSupprimer" . '>Supprimer liste</a>';
				$affichProprietaire = '<p>Vous êtes le propriétaire de cette liste</p>';
			} else {
				if (isset($createur)) {
					$nomProprietaire = User::where('uid', '=', $liste->user_id)->first()->username;
					$affichProprietaire = '<p>Liste créée par ' . "$nomProprietaire" . '</p>';
				} else {
					$affichProprietaire = "<p>Utilisateur inconnu</p>";
				}
			}
            
			$html .= <<<END

				<li class="card">
					<h3>$liste->titre</h3>
					$affichProprietaire
					<p>$liste->description</p>
					<div class="card_footer">
						<p class="expiration">Expire le $liste->expiration</p>
						<a href="$voirItems">Voir les items</a>
                        $modifButton
                        $supprButton
					</div>
				</li>
END;
		}
		$html .= "
			</ul>
		</div>";
		return $html;
	}
	
}
