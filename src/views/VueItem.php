<?php
namespace mywishlist\views;

use mywishlist\models\Liste;
use mywishlist\models\Reservation;

class VueItem extends VueAbstraite {
	
	private $item;
	
	public function __construct($item) {
		$this->item = $item;
	}
	
	public function render() {
		$app = \Slim\Slim::getInstance();
		$no = $this->item->liste_id;
		$item = $this->item;
		$id_item = $item->id;
		$url_retour = $app->urlFor('listeParId', ['id'=>$no]);
		$urlModification = $app->urlFor('modifItem', ['id'=>$id_item]);
		$urlSupprimer = $app->urlFor('suppressionItem', ['id'=>$id_item]);
		$urlReserver=$app->urlFor('formulairereservationItem', ['id'=>$id_item]);
		$liste = Liste::where('no', '=', $item->liste_id)->first();
		$btnRetour = "";
		if (isset($_SESSION['profile']) || $liste->status == 'public') {
			$btnRetour = <<<END
			<a href="$url_retour" class="retour">
				<i class="material-icons">keyboard_arrow_left</i>
				<div>Retour aux items</div>
			</a>
END;
		}
		
		$chgImgButton = "";
		$modifButton = "";
		$supprButton = "";
		$reservButton="";
		$urlModifImage = $app->urlFor('modificationImageItem', ['id'=>$id_item]);
		$urlSupprImage = $app->urlFor('suppressionImageItem', ['id'=>$id_item]);
		
		$reservation = Reservation::where('item_id', '=', $id_item)->first();
		$msgReserv = "Item non réservé";
		if (isset($reservation)) {
			$nomReserv = $reservation->nom;
			$prenomReserv = $reservation->prenom;
			$message = $reservation->message;
			$msgReserv = "Item réservé par $prenomReserv $nomReserv : « $message »";
		}
		if (isset ($_SESSION['profile']) && $_SESSION['profile']['uid'] == $liste->user_id) {
			$chgImgButton = '<button class="edit_img" onclick="showBox(\'img_box\')">
				<i class="material-icons">edit</i><span>Modifier l\'image</span>
			</button>';
			
			$modifButton = '<a href=' . "$urlModification" . '">Modifier item</a>';
			$supprButton = '<a href=' . "$urlSupprimer" . '>Supprimer item</a>';
		}
		if (isset($reservation)) {
			$reservButton = '<a>Item réservé</a>';
		} else {
			$reservButton = '<a href=' . "$urlReserver" . '>Réserver item</a>';
		}
		$html = <<<END

		<div class="container">
			$btnRetour
			<div class="card item">
				<h3 class="nom_item">$item->nom</h3>
				<div class="item_img_wrapper">
					<img src="../web/img/$item->img">
					$chgImgButton
				</div>
				<p>$item->descr</p>
                <p>$msgReserv</p>
				<p class="prix">$item->tarif €</p>
				<div class="card_footer">
					$modifButton
					$supprButton
					$reservButton
				</div>
			</div>
		</div>
		
		<div id="img_box" class="lightbox">
			<div class="lightbox_header">
				<h2>Modifier l'image de cet item</h2>
				<button onclick="closeBox('img_box')" class="close">
					<i class="material-icons">close</i>
				</button>
			</div>
			<form method="POST" action="$urlModifImage" class="card" enctype="multipart/form-data">				
				<label for="input_file">Image de votre ordinateur</label>
				<div class="input_wrapper">
					<input id="input_file" type="file" name="image_remplacement" accept="image/*">
					<div id="file_path" class="path">Aucune image</div>
				</div>
				<div class="card_footer">
					<a href="$urlSupprImage">Supprimer l'image</a>
					<button type="submit">Remplacer par cette image</button>
				</div>
			</form>
		</div>
		
END;
		return $html;
	}
	
}
