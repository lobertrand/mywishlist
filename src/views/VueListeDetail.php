<?php
namespace mywishlist\views;

use mywishlist\models\User;

class VueListeDetail extends VueAbstraite {
	
	private $items;
	private $liste;
	private $coms;
	
	public function __construct($liste, $items, $coms) {
		$this->items = $items;
		$this->liste = $liste;
		$this->coms = $coms;
	}
	
	public function render() {
		$app = \Slim\Slim::getInstance();
		$liste = $this->liste;
		$affichProprietaire = "";
		if($liste->status != 'private'){
			if (isset($_SESSION['profile']) && $_SESSION['profile']['uid'] == $liste->user_id) {
				$affichProprietaire = '<p>Vous êtes le propriétaire de cette liste</p>';
			} else {
				$proprietaire = User::where('uid', '=', $liste->user_id)->first();
				$nom = isset($proprietaire) ? $proprietaire->username : "Unknown";
				$affichProprietaire = '<p>' . "$nom" . ' est le propriétaire de cette liste</p>';
			}
		}
		$affichageListeVide = "";
		if ($this->items->first() == null) {
			$affichageListeVide = '<p>Cette liste ne comporte pas d\'item</p>';
		}
		
		// Informations sur la liste
		$html = <<<END

		<div class="container">
			<h2>Liste "$liste->titre"</h2>			
		
			<div class="card">
				$affichProprietaire
				<p class="expiration">Cette liste expirera le $liste->expiration</p>
				<h3>Description</h3>
				<p>$liste->description</p>
				<h3>Items de la liste</h3>
				$affichageListeVide
				<div class="table">	
END;

		foreach ($this->items as $it) {
			$url_item = $app->urlFor("itemParId", ["id"=>$it->id]);
			$url_suppr = $app->urlFor('suppressionItem', ['id'=>$it->id]);
			$supprButton = "";
			if (isset($_SESSION['profile']) && $_SESSION['profile']['uid'] == $this->liste->user_id) {
				$supprButton = '<a class="data btn" href=' . "$url_suppr" . '><i class="material-icons">delete</i><span>Supprimer</span></a>';
			}
			
			$html .= <<<END
					<div class="row">
						<div class="data">
							<div class="img_wrapper" style="background-image:url(../web/img/$it->img)"></div>
						</div>
						<a href="$url_item" class="data">$it->nom</a>
						$supprButton
					</div>
END;
		}
		$liste_id = $this->liste->no;
		$ajout = $app->urlFor('ajoutitem', ['liste_id'=>$liste_id]);
		$ajoutButton = "";
		if (isset($_SESSION['profile']) && $_SESSION['profile']['uid'] == $this->liste->user_id) {
			$ajoutButton = '<a class="row" href=' . "$ajout" . '><div class="data"><i class="material-icons add_item">add_circle</i></div><div class="data">Ajouter un item</div></a>';
		}
		$html .= <<<END
				$ajoutButton
				</div>
				<h3>Commentaires</h3>
END;
		foreach ($this->coms as $com) {
			$auteur = $com->auteur()->first()->username;
			$url_suppr = $app->urlFor('suppressionCommentaire', ['id'=>$com->id]);
			$supprBtn = "";
			if (isset($_SESSION['profile']) && $_SESSION['profile']['uid'] == $com->user_id) {
				$supprBtn = '<a class="suppr_com" href="' . $url_suppr. '">Supprimer</a>';
			}
			
			$html .= <<<END
				<div>
					<p class="auteur_com">$auteur</p>
					<p class="com">$com->content</p>
					<p class="publication">$com->publication</p>
					$supprBtn
				</div>
END;
		}
		
		if (isset($_SESSION['profile'])) {
		$ajoutCom = $app->urlFor('ajoutCommentaire', ['id'=>$liste_id]);
		$html .= <<<END
				<form id="f2" method="post" action="$ajoutCom">
					<textarea name="commentaire" maxlength="240"></textarea>

					<div class="card_footer">
						<button type="submit">Commenter</button>
					</div>
				</form>
			</div>
		</div>
END;
		}
		return $html;
	}
	
}
