<?php 

	$app = \Slim\Slim::getInstance();
	$conn = $app->urlFor('inscription');
	$accueil = $app->urlFor('connexion');
	$deconnect = $app->urlFor('deconnexion');
	$css = 'web/css/style.css';
	$script = 'web/js/script.js';
	
	// Boutons <Se connecter> ou <Mes listes><Se déconnecter>
	$btn_conn = "";
	$uname;
	if (isset($_SESSION['profile'])) {
		$uname = $_SESSION['profile']['username'];
		$mes_listes = $app->urlFor('listes');
		$btns_conn = <<<END
		<a href="$mes_listes" class="btn" autofocus>
			<i class="material-icons">account_circle</i>
			<span>Mon espace perso</span>
		</a>
		<a href="$deconnect" class="btn">
			<i class="material-icons">close</i>
			<span>Déconnexion</span>
		</a>
		<p>Connecté en tant que $uname</p>
END;
	} else {
		$btns_conn = <<<END
		<button class="btn" onclick="showBox('conn_box')" autofocus>
			<i class="material-icons">account_circle</i>
			<span>Se connecter</span>
		</button>
		<button class="btn" onclick="showBox('insc_box')">
			<i class="material-icons">person_add</i>
			<span>Créer un compte</span>
		</button>
END;
	}
	
?>


<!DOCTYPE html>
<html lang="fr">

<head>
	<title>MyWishList</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="<?php echo $css?>" type="text/css">
	<link rel="icon" type="image/png" href="web/img/favicon.png" />
</head>

<body class="safe_background">
	<div class="landing_page">
		<header>
			<div class="prlx" prlxImg="web/img/mountains.jpg" prlxOffset="350"></div>
			<div class="prlx" prlxImg="web/img/mountains2.png" prlxOffset="300"></div>
			<div class="prlx" prlxImg="web/img/mountains1.png" prlxOffset="200"></div>
			<div class="container">
				<h1>MyWishList</h1>
				<p>Créez des listes de souhaits et partagez-les avec vos proches !</p>
				<nav>
					<?php echo $btns_conn ?>
				</nav>
			</div>
		</header>
	</div>

	<div id="insc_box" class="lightbox">
		<div class="lightbox_header">
			<h2>Inscription</h2>
			<button onclick="closeBox('insc_box')" class="close">
				<i class="material-icons">close</i>
			</button>
		</div>
		<form id="form_inscription" method="POST" action="<?php echo $conn ?>" class="card">
			<label for="insc_name">Choisissez un identifiant<br>(minimum 4 lettres ou chiffres)</label>
			<input type="text" name="username" id="insc_name" required>
			<label for="insc_pass">Choissez un mot de passe<br>(minimum 6 lettres ou chiffres)</label>
			<input type="password" name="password" id="insc_pass" required>
			<label for="confirm_pass">Confirmez le mot de passe</label>
			<input type="password" name="confirm_password" id="confirm_pass" required>
			<div class="card_footer">
				<button type="submit">S'inscrire</button>
			</div>
		</form>
	</div>

	<div id="conn_box" class="lightbox">
		<div class="lightbox_header">
			<h2>Connexion</h2>
			<button onclick="closeBox('conn_box')" class="close">
				<i class="material-icons">close</i>
			</button>
		</div>
		<form id="form_connexion" method="POST" action="<?php echo $accueil ?>" class="card">
			<label for="user_name">Identifiant</label>
			<input type="text" name="username" id="user_name" required>
			<label for="user_pass">Mot de passe</label>
			<input type="password" name="password" id="user_pass" required>
			<div class="card_footer">
				<button type="submit">Se connecter</button>
			</div>
		</form>
	</div>
	
	<div class="main_content">
	
		<section>
			<div class="container">
				<p>Découvrez ci-dessous les listes publiques, accessibles sans inscription !<p>
			</div>
		</section>
		
		<section class="listes_pub">
			<div class="container">
				<h2>Listes de souhaits publiques</h2>
				<?php foreach($this->listes_publiques as $liste): ?>
					<div class="card">
						<h3><?php echo $liste->titre ?></h3>
						<div class="card_footer">
							<p class="expiration">Expire le <?php echo $liste->expiration ?></p>
							<?php
								
								$no = $liste->no;
								$voirItems = $app->urlFor('listeParId', ['id'=>$no]);
								echo '<a href=' . "$voirItems" . '>Voir les items</a>';
								
							?>
							<div style="clear: both"></div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</section>

		<section>
			<div class="container">
				<h2>Découvrez MyWishlist</h2>
				<p>  MyWishList est un site web permettant de gérer des listes de souhaits en ligne. Vous pouvez créer un compte gratuitement afin d'éditer des listes comportant les items que vous souhaitez. Vous pouvez ensuite les partager à vos proches, qui pourront proposer de financer un item de la liste. Vous pouvez également créer des listes publiques, visibles sur l'accueil du site.<p>
			</div>
		</section>
		
		<section>
			<div class="container">
					<p>Site web créé par Loïc Bertrand, Ismail Agbani, Marius Corbière et Yassin Moutaoukil</p>
			</div>
		</section>
	</div>

	<script src="<?php echo $script?>" type="text/javascript"></script>
</body>

</html>
