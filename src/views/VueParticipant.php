<?php
namespace mywishlist\views;

use mywishlist\controllers\PathFinder;

class VueParticipant extends VueAbstraite
{

    const VUE_LISTES = 1;

    const VUE_LISTEDETAIL = 2;

    const VUE_ITEM = 3;

    const VUE_LISTESPUBLIQUES = 4;
    
    const VUE_FORMRESERVATION = 5;

    private $tab_obj;

    private $rendering_type;

    public function __construct($tab_obj, $rendering_type)
    {
        $this->tab_obj = $tab_obj;
        $this->rendering_type = $rendering_type;
    }

    public function render()
    {
        $content;
        switch ($this->rendering_type) {
            case self::VUE_LISTES:
                $v = new VueListes($this->tab_obj[0]);
                $content = $v->render();
                break;
            case self::VUE_LISTEDETAIL:
                $v = new VueListeDetail($this->tab_obj[0], $this->tab_obj[1], $this->tab_obj[2]);
                $content = $v->render();
                break;
            case self::VUE_ITEM:
                $v = new VueItem($this->tab_obj[0]);
                $content = $v->render();
                break;         
            case self::VUE_LISTESPUBLIQUES:
				$v = new VueListesPubliques($this->tab_obj[0]);
				$content = $v->render();
				break;
            case self::VUE_FORMRESERVATION:
                $v = new VueFormulaireReservation($this->tab_obj[0]);
                $content = $v->render();
                break;
        }
        $app = \Slim\Slim::getInstance();
        
        $css = PathFinder::findPath('web/css/style.css');
        $script = PathFinder::findPath('web/js/script.js');
        $favicon = PathFinder::findPath('web/img/favicon.png');
        $listes_publiques = $app->urlFor('listespubliques');
        $username = isset($_SESSION['profile']) ? $_SESSION['profile']['username'] : "";
        $accueil = $app->urlFor('accueil');
        $page_globale = <<<END
		<!DOCTYPE html>
		<html lang="fr">
    		<head>
				<title>MyWishList</title>
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
				<link rel="stylesheet" href="$css" type="text/css">
				<script src="$script" type="text/javascript" async></script>
				<link rel="icon" type="image/png" href="$favicon" />
			</head>
			<body>
			<div class="top_page">
				<header id="header">
					<div class="container">
						<h1 id="header_title">MyWishList</h1>
					</div>
				</header>
				<nav>
					<ul class="container">
                        <li><a href="$accueil">
							<i class="material-icons">home</i>
							<div>Accueil</div>
						</a></li>
						<li><a href="$listes_publiques">
							<i class="material-icons">view_list</i>
							<div>Listes publiques</div>
						</a></li>
					</ul>
				</nav>
			</div>
			<main class="content">
    			$content
			</main>	
    		</body>
		</html>
END;
        
        echo $page_globale;
    }
}
