<?php
namespace mywishlist\views;

class VueAccueil extends VueAbstraite {

	private $listes_publiques;

	public function __construct($listes_publiques) {
		$this->listes_publiques = $listes_publiques;
	}

	public function render() {
		$v = new VueListesPubliques($this->listes_publiques);
		$render_listes_publiques = $v->render(); // retourne du code html
		include('accueil.php');
	}
}
