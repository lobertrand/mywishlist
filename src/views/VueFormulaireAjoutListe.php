<?php
namespace mywishlist\views;

class VueFormulaireAJoutListe extends VueAbstraite
{

    public function __construct() {}

    public function render()
    {
        $app = \Slim\Slim::getInstance();
		$creation = $app->urlFor("creationListe");	
		$html = <<<END

		<div class="container">

			<h2>Ajouter une nouvelle liste</h2>
			<form id="f2" method="post" action="$creation" class="card">
								
				<label for="f2_nom">Nom</label>
				<input type="text" name="nomListe" required autofocus>

				<label for="f2_descr">Description</label>
				<textarea name="descrListe" maxlength="240" required></textarea>

				<label for="f2_expiration">Date d'expiration</label>
				<input type="date" name="expiration">
				
				<!-- 
					Le label des boutons radios doit être en dessous de
					l'input pour pouvoir ajouter du css
				//-->
				
				<div class="radio_btn">
					<input id="f2_prive" type= "radio" name="status" value="private" checked>
					<label for="f2_prive"><span>Privé</span></label>
				</div>
				<div class="radio_btn">
					<input id="f2_public" type= "radio" name="status" value="public">
					<label for="f2_public"><span>Public</span></label>
				</div>

				<div class="card_footer">
					<button type="submit">Valider</button>
				</div>
			</form>
		</div>
		
END;
        return $html;
    }
}
