<?php
namespace mywishlist\views;

class VueFormulaireReservation extends VueAbstraite
{
    
    private $item;
    
    public function __construct($item){
        $this->item = $item;
    }
    
    public function render()
    {
        $app = \Slim\Slim::getInstance();
        $id = $this->item->id;
        $item = $this->item;
        $reservation = $app->urlFor("reservationItem", ["id"=>$id]);
        $html = <<<END
        
		<div class="container">
		
			<h2>Reservation</h2>
			<form id="f2" method="post" action="$reservation" class="card">
			
				<label for="f1_name">Nom</label>
				<input type="text" name="Nom" value="" required autofocus>
				
				<label for="f1_descr">Prenom</label>
				<input type="text" name="Prenom" value="" required>
				
				<label for="f1_descr">Message</label>
				<input type="text" name="Message" value="" required>
				
				
				<div class="card_footer">
					<button type="submit">Valider</button>
				</div>
			</form>
		</div>
		
END;
        return $html;
    }
}
