<?php
namespace mywishlist\views;

class VueCompte extends VueAbstraite {
	
	public function __construct() {
		
	}

	public function render() {
		$app = \Slim\Slim::getInstance();
		$pseudo_actuel = $_SESSION['profile']['username'];
		$chgmt_mdp = $app->urlFor('changementMotDePasse');
		$suppr = $app->urlFor('suppressionCompte');
		
		$html = <<<END
		<div class="container">
			<h2>Paramètres de mon compte</h2>
			
			<form class="card" method="POST" action="$chgmt_mdp">
				<h3>Changer mon mot de passe</h3>
				
				<label for="ancien">Ancien mot de passe</label>
				<input type="password" name="ancien" required>
				
				<label for="nouveau">Nouveau mot de passe</label>
				<input type="password" name="nouveau" required>
				
				<label for="confirm">Confirmation du mot de passe</label>
				<input type="password" name="confirm" required>
				
				<div class="card_footer">
					<button type="submit">Valider le changement</button>
				</div>
			</form>
			
			<form class="card" method="POST" action="$suppr">
				<h3>Supprimer mon compte</h3>
				
				<label for="pass">Mot de passe requis</label>
				<input type="password" name="pass" required>
				
				<div class="card_footer">
					<button class="warning" type="submit">Supprimer définitivement</button>
				</div>
			</form>
			
		</div>
END;
		return $html;
	}
	
}
