<?php
namespace mywishlist\views;

class VueFormulaireItem extends VueAbstraite {

	private $liste;
	
    public function __construct($liste) {
		$this->liste = $liste;
	}

    public function render() {
        $app = \Slim\Slim::getInstance();
		$liste_id = $this->liste->no;
        $creation = $app->urlFor("insertionitem", ['liste_id'=>$liste_id]);
        $html = <<<END

		<div class="container">

			<h2>Ajouter un nouvel item</h2>

			<form method="post" action="$creation" enctype="multipart/form-data" class="card">

				<label for="name">Nom</label>
				<input id="name" type="text" name="nomItem" required autofocus>
				
				<label for="descr">Description</label>
				<input id="descr" type="text" name="descrItem" required>
				
				<!--<label for="f1_url">Url</label>
				<input type="url" name="url">
				
				<p>ou</p>//-->
				
				<label for="input_file">Image de votre ordinateur</label>
				<div class="input_wrapper">
					<input id="input_file" type="file" name="image" accept="image/*">
					<div id="file_path" class="path">Aucune image</div>
				</div>
				
				<label for="tarif">Tarif</label>
				<input id="tarif" type="number" step="0.01" name="tarif" required>
				
				<div class="card_footer">
					<button type="submit">Valider</button>
				</div>
				
			</form>	
		</div>
		
END;
        return $html;
    }
}