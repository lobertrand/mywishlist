<?php
namespace mywishlist\views;

class VueFormulaireModifListe extends VueAbstraite
{

	private $liste;
	
    public function __construct($liste) {
		$this->liste = $liste;
	}

    public function render()
    {
        $app = \Slim\Slim::getInstance();
		$no = $this->liste->no;
		$modification = $app->urlFor("modificationliste", ['no'=>$no]);
		$liste = $this->liste;
		
		if($liste->status != 'private'){
			$radio1 = '<input id="f2_prive" type= "radio" name="status" value="private">';
			$radio2 = '<input id="f2_public" type= "radio" name="status" value="public" checked>';
		} else {
			$radio1 = '<input id="f2_prive" type= "radio" name="status" value="private" checked>';
			$radio2 = '<input id="f2_public" type= "radio" name="status" value="public">';
		}
		
		$html = <<<END

		<div class="container">

			<h2>Modifier la liste</h2>
			<form id="f2" method="post" action="$modification" class="card">
				
				<label for="f2_nom">Titre</label>
				<input type="text" name="nomListe" value="$liste->titre" required autofocus>

				<label for="f2_descr">Description</label>
				<textarea name="descrListe" maxlength="240">$liste->description</textarea>

				<label for="f2_expiration">Date d'expiration</label>
				<input type="date" name="expiration" value="$liste->expiration">
				
				<div class="radio_btn">
					$radio1
					<label for="f2_prive"><span>Privé</span></label>
				</div>
				<div class="radio_btn">
					$radio2
					<label for="f2_public"><span>Public</span></label>
				</div>
				
				<div class="card_footer">
					<button type="submit">Valider</button>
				</div>
			</form>
		</div>
		
END;
        return $html;
    }
}
