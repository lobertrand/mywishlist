// HEADER //

const title = document.getElementById('header_title');
const maxHeight = 130;
const minHeight = 60;

function resizeHeaderOnScroll() {
	if (title) {
		const distanceY = window.pageYOffset || document.documentElement.scrollTop;

		if (distanceY < maxHeight - minHeight) {
			height = maxHeight - distanceY;
			title.style.height = height + "px";
			title.style.lineHeight = height + "px";
		} else {
			title.style.height = minHeight + "px";
			title.style.lineHeight = minHeight + "px";
		}		
	}
}

window.addEventListener('scroll', resizeHeaderOnScroll);

// INPUT FILES //

const inputFile = document.getElementById("input_file");
const filePath = document.getElementById("file_path");

if (inputFile && filePath) {
	inputFile.addEventListener("change", function() {
		path = inputFile.files[0].name;
		console.log(path);
		filePath.innerHTML = path;
	}, false);
}

// IMAGE CHANGING LIGHTBOX //

let lastShown;

function showBox(id) {
	const box = document.getElementById(id);
	box.classList.add('active');
	lastShown = box;
}

function closeBox(id) {
	const box = document.getElementById(id);
	box.classList.remove('active');
}

// Fermer la boîte en cliquant sur le fond noir
window.onclick = function(event) {
    if (event.target == lastShown) {
        closeBox(lastShown.id);
    }
}

// PARALLAX //

const parallaxes = document.getElementsByClassName("prlx");

for (let i = 0; i < parallaxes.length; i++) {
	const p = parallaxes[i];
	p.style.backgroundImage = "url(" + p.getAttribute("prlxImg") + ")";
	p.style.backgroundSize = "cover";
	p.style.backgroundPosition = "center";
	p.style.position = "absolute";
	p.style.top = "-" + p.getAttribute("prlxOffset") + "px" | "0";
	p.style.right = "0";
	p.style.bottom = "0";
	p.style.left = "0";
	p.style.zIndex = "-1";
	p.style.transition = "0";
}

function doTheParallax() {
	for (let i = 0; i < parallaxes.length; i++) {
		const p = parallaxes[i];
		const offset = p.getAttribute("prlxOffset");
		const height = p.clientHeight;
		const dY = constrain(window.pageYOffset || document.documentElement.scrollTop, height);
		const trans = map(dY, 0, height, 0, offset);
		p.style.transform = "translateY(" + trans + "px)";
	}
}

window.addEventListener('scroll', doTheParallax);

function map(val, min1, max1, min2, max2) {
	return min2 + (max2 - min2) * ((val - min1) / (max1 - min1));
}

function constrain(val, max) {
	return val > max ? max : val; 
}