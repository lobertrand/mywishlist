# README #

# Projet MyWishList 2017-2018

## Groupe S3D

* Ismail AGBANI
* Loïc BERTRAND
* Marius CORBIÈRE
* Yassin MOUTAOUKIL

## Instructions

### Site hébergé sur Webetu
* https://webetu.iutnc.univ-lorraine.fr/www/bertra182u/mywishlist/

### Installation
* Cloner le dépot git à la racine de votre serveur (wampp, xampp...)
* La base de donnée associée doit être installée en exécutant le script SQL tables.sql
* Créer un fichier de configuration conf.ini à la racine du projet, contenant l'indentifiant et le mot de passe de la base de données 

### Comptes diponibles
* pseudo : coucou, mot de passe : coucou
* pseudo : heyhey, mot de passe : heyhey