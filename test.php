<?php

phpinfo();
exit();

require_once 'vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use mywishlist\models\Liste;
use mywishlist\models\Item;

// Connexion
$db = new DB();

$username = parse_ini_file('src/conf/conf.ini')['username'];
$password = parse_ini_file('src/conf/conf.ini')['password'];

$db->addConnection([
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'bertra182u',
    'username' => $username,
    'password' => $password,
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
]);

$db->setAsGlobal();
$db->bootEloquent();

// Tests

// Liste les listes
$listes = Liste::get();
echo "<h1>Liste des souhaits</h1><ul>";
foreach ($listes as $liste) {
    echo "<li>$liste->titre</li>";
}
echo "</ul>";

// Liste les items
$items = Item::get();
echo "<h1>Liste des items</h1><ul>";
foreach ($items as $item) {
    echo "<li>$item->nom : ";
    echo $item->liste->titre;
    echo "</li>";
}
echo "</ul>";

// Affiche un item dont l'id est pass� en param�tre dans l'URL
$id = $_GET["id"];
$item1 = Item::where('id', "=", $id)->first()->nom;
echo "<h1>Item par id</h1><p>$item1</p>";

// Cr�� un item, l'ins�re dans la base et l'ajoute � une liste de souhaits
$it = new Item();
$it->liste_id = 3;
$it->nom = 'Sudoku';
$it->descr = 'Du sudoku pour s\'amuser tout en se cassant la tete';
$it->img = 'sudoku.jpg';
$it->tarif = 10.00;
// echo $it->save();

// Liste les items d'une liste donn�e dont l'id est pass�e en param�tre
$id = $_GET["id"];
$liste1 = Liste::where('no', "=", $id)->first();
$l_items = $liste1->items;
echo "<h1>Liste des items</h1><ul>";
foreach ($l_items as $item) {
    echo "<li>$item->nom</li>";
}
echo "</ul>";








